import setuptools
import os

requirements = list()
requirements_file = 'requirements.txt'
if os.access(requirements_file, os.R_OK):
    with open(requirements_file, 'r') as requirements_file_pointer:
        requirements = requirements_file_pointer.read().split()
setuptools.setup(
    scripts=['image_classifier/image_classifier.py'],
    author="Antonio J. Delgado",
    version='0.0.15',
    name='image_classifier',
    author_email="antoniodelgado@susurrando.com",
    url="",
    description="""Find faces in images and add the names as metadata to the
images. Move images to a folder withe the structure YYYY/MM/DD.""",
    license="GPLv3",
    install_requires=requirements,
    keywords=[
        "face-recognition",
        "face",
        "recognition",
        "classify",
        "images",
        "metadata"
    ]
)
