== image_classifier

Classifies images by their EXIF date (e.g. /path/YYYY.MM.DD/image.jpg).

= Install

apt install exiv2 cmake libexiv2-dev  libboost-python-dev
python3 setup.py install
